(ns com.zeddworks.double-booked
  (:require [java-time :as java-time]
            [clojure.java.io :as io]
            [clojure.pprint :as pprint])
  (:gen-class))

(defn overlaps?
  "Returns true if interval `i` and other interval `oi` overlap and are
   not the same instance, otherwise returns false."
  [i oi]
  (let [not-identical (complement identical?)]
    (and (not-identical i oi)
         (java-time/overlaps? i oi))))

(defn truthy-overlaps
  "Returns pair of interval `i` and other interval `oi` when they
   overlap and are not the same instance, otherwise returns nil."
  [i oi]
  (when (overlaps? i oi)
    [i oi]))

(defn intervals->strings
  "Converts `org.threeten.extra.Interval` pair to string pair."
  [[i oi]]
  [(str i)
   (str oi)])

(defn double-booked
  [xs]
  (let [intervals (map java-time/interval xs)
        overlaps (for [i intervals
                       oi intervals]
                   (truthy-overlaps i oi))]
    (into []
          (comp (keep identity)
                (map intervals->strings))
          overlaps)))

(defn double-booked-alternate
  "An alternate implementation of `double-booked`"
  [xs]
  (let [intervals (map java-time/interval xs)
        overlaps (fn overlaps [i]
                   (keep (partial truthy-overlaps i)
                         intervals))]
    (into []
          (comp (mapcat overlaps)
                (map intervals->strings))
          intervals)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (if-let [filename (first args)]
    (-> filename
        io/resource
        slurp
        read-string
        double-booked
        clojure.pprint/pprint)))
