(ns com.zeddworks.double-booked-test
  (:require [clojure.test :refer :all]
            [java-time :as java-time]
            [com.zeddworks.double-booked :as sut]))

(deftest truthy-overlaps-test
  (are [input expected] (let [[i oi] (map java-time/interval input)]
                          (= expected
                             (when-let [overlaps (sut/truthy-overlaps i oi)]
                               (sut/intervals->strings overlaps))))
    ["2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"
     "2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"]
    ["2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"
     "2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"]

    ["2019-01-14T05:00:00Z/2019-01-16T05:00:00Z"
     "2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"]
    nil

    ["2019-01-14T05:00:00Z/2019-01-16T05:00:00Z"
     "2019-01-14T05:00:00Z/2019-01-16T05:00:00Z"]
    ["2019-01-14T05:00:00Z/2019-01-16T05:00:00Z"
     "2019-01-14T05:00:00Z/2019-01-16T05:00:00Z"]))

(deftest double-booked-test
  (let [input-data ["2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"
                    "2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"
                    "2019-01-18T05:00:00Z/2019-01-23T05:00:00Z"
                    "2019-01-14T05:00:00Z/2019-01-16T05:00:00Z"]
        expected-overlaps [["2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"
                            "2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"]
                           ["2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"
                            "2019-01-18T05:00:00Z/2019-01-23T05:00:00Z"]
                           ["2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"
                            "2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"]
                           ["2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"
                            "2019-01-18T05:00:00Z/2019-01-23T05:00:00Z"]
                           ["2019-01-18T05:00:00Z/2019-01-23T05:00:00Z"
                            "2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"]
                           ["2019-01-18T05:00:00Z/2019-01-23T05:00:00Z"
                            "2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"]]]
    (is (= expected-overlaps (sut/double-booked input-data)))
    (is (= expected-overlaps (sut/double-booked-alternate input-data)))))
