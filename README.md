# double-booked

## Environment
- Java: openjdk-11.0.1
- Clojure: 1.10.0
- Requires installation of "Command Line Tools" available at [https://clojure.org/guides/getting_started](Getting
Started)
  - Optionally if using mac or linux, one can use the [https://github.com/asdf-vm/asdf](ASDF Version
  Manager) to install the Clojure Command Line Tools using the [https://github.com/halcyon/asdf-clojure.git](ASDF
  Clojure plugin)

## Usage

Run the project directly from within the project's root:

*Please note filename is local to the resources directory*

``` shell
$ clj -m com.zeddworks.double-booked [filename]
```

Run the project's tests:

``` shell
$ clj -A:test:runner
```

## Examples

``` edn
% cat resources/input.edn
["2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"
 "2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"
 "2019-01-18T05:00:00Z/2019-01-23T05:00:00Z"
 "2019-01-14T05:00:00Z/2019-01-16T05:00:00Z"]
```


``` edn
% clj -m com.zeddworks.double-booked input.edn
[["2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"
  "2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"]
 ["2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"
  "2019-01-18T05:00:00Z/2019-01-23T05:00:00Z"]
 ["2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"
  "2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"]
 ["2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"
  "2019-01-18T05:00:00Z/2019-01-23T05:00:00Z"]
 ["2019-01-18T05:00:00Z/2019-01-23T05:00:00Z"
  "2019-01-20T05:00:00Z/2019-01-21T06:00:00Z"]
 ["2019-01-18T05:00:00Z/2019-01-23T05:00:00Z"
  "2019-01-20T04:00:00Z/2019-01-20T10:00:00Z"]]
```

``` edn
% clj -A:test:runner

Running tests in #{"test"}

Testing com.zeddworks.double-booked-test

Ran 2 tests containing 5 assertions.
0 failures, 0 errors.
```

## License

Copyright © 2019 Scott McLeod
